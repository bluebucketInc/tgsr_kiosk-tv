import React from 'react';
import { Switch, Route } from 'react-router';

// pages
import BoomBox from './containers/boombox';
import VoteOverlay from './containers/vote-overlay';
import OnboardingScreens from './containers/onboarding';
import { WhyVote, WinPrizes } from './containers/content-popup';

// app
export default class Routes extends React.Component {
  // render method
  render() {
    return (
      <Switch>
        <Route exact path="/" component={BoomBox} />
        <Route exact path="/vote" component={VoteOverlay} />
        <Route exact path="/why-vote" component={WhyVote} />
        <Route exact path="/how-to-win" component={WinPrizes} />
        <Route exact path="/how-to-play" component={OnboardingScreens} />
      </Switch>
    );
  }
}
