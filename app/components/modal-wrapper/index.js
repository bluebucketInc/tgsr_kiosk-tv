/*global $*/

// imports
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import Rodal from 'rodal'

// include styles
import './styles.global.scss'

// component
const ModalWrapper = ({ isActive, onClose, children, to, className }) => {
    // change
    // useEffect(() => {
    //     isActive
    //         ? $('body').addClass('popup-active')
    //         : $('body').removeClass('popup-active')
    // }, [isActive])

    // render
    return (
        <Rodal
            width={100}
            height={100}
            measure="%"
            visible={isActive}
            onClose={onClose}
            closeMaskOnClick={false}
            animation="fade"
            leaveAnimation="slideDown"
            duration={600}
            className={`modal-wrapper ${className}`}
            showCloseButton={false}>
            <div className="tgsr-logo">
                <a href="/">
                    <img
                        alt="TGSR"
                        src="/clientlibs/images/global/tgsr_logo.gif"
                    />
                </a>
            </div>
            {to ? (
                <Link to="/" onClick={onClose} style={{ position: 'initial' }}>
                    <img
                        src={require(`./assets/close-btn-mobile.png`)}
                        alt="close button"
                        className="close-btn"
                    />
                </Link>
            ) : 
            <img
                    onClick={onClose}
                    src={require(`./assets/close-btn-mobile.png`)}
                    alt="close button"
                    className="close-btn"
                />
            }
            {children}
        </Rodal>
    )
}

// export
export default ModalWrapper
