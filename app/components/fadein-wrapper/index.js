// imports
import React from 'react'
import { Animated } from 'react-animated-css'

// components
import { AnimateDuration } from '../../utils/settings'

// component
const FadeInWrapper = ({ classes, children, isVisible }) => {
    return (
        <Animated
            animationInDuration={AnimateDuration}
            animationOutDuration={AnimateDuration}
            className={classes}
            animationIn="fadeIn"
            animationOut="fadeOut"
            isVisible={isVisible}>
            {children}
        </Animated>
    )
}

// export
export default FadeInWrapper
