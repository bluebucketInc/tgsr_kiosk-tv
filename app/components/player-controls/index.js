// imports
import React from 'react'

// styles
import './styles.global.scss'

// component
const PlayerLabel = ({
    isPlaying,
    onPlay,
    onPause,
    onRandom,
    disableRandom,
}) => {
    return (
        <React.Fragment>
            {/* Play / Pause Buttons */}
            <div className="btns-container">
                <div className="controls">
                    {isPlaying ? (
                        <img
                            onClick={() => onPause()}
                            className="pause"
                            src={require('./assets/btn_paused.png')}
                            alt="Pause"
                        />
                    ) : (
                        <img
                            onClick={() => onPlay()}
                            className="play"
                            src={require('./assets/btn_play.png')}
                            alt="Play"
                        />
                    )}
                </div>
                <img
                    onClick={() => onRandom()}
                    className={`randomizer ${disableRandom ? 'disabled' : ''}`}
                    src={require('./assets/btn_randomise.png')}
                    alt="Randomize"
                />
            </div>
        </React.Fragment>
    )
}

// export
export default PlayerLabel
