// imports
import React from 'react'

// styles
import './styles.global.scss'

// quiz button
export const Button = ({ onClick, children, classes, type, id }) => (
    <button
    	id={id}
        className={`button raised ${classes ? classes : ''}`}
        onClick={onClick}
        type={type}>
        {children}
    </button>
)
