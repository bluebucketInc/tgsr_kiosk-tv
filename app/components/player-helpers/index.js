// imports
import React from 'react'
import { Link } from 'react-router-dom'
import { IndextoGenre } from '../../utils'

// components
import { Button } from '../button'

// styles
import './styles.global.scss'

// component
const PlayerHelpers = ({ hasPlayed, left, right }) => {
    return (
        <React.Fragment>
            {/* Bottom Controls */}
            <div className="bottomctrls-container">
                <Link
                    onClick={() => window.dataLayer.push({
                        vote:
                            IndextoGenre(left, 'left') +
                            '-' +
                            IndextoGenre(right, 'right'),
                    })}
                    to="/vote"
                    className={`wrapped-button ${
                        hasPlayed ? '' : 'disabled-link'
                    }`}>
                    <Button id="vote-btn" classes={hasPlayed ? '' : 'disabled'}>
                        Vote
                    </Button>
                </Link>
                <div className="helper-links">
                    <p>
                        <Link to="/why-vote">Why Vote?</Link>
                    </p>
                    <span>|</span>
                    <p>
                        <Link to="/how-to-play">How to Play?</Link>
                    </p>
                    <span>|</span>
                    <p>
                        <Link to="/how-to-win">How to Win Prizes?</Link>
                    </p>
                </div>
            </div>
        </React.Fragment>
    )
}

// export
export default PlayerHelpers
