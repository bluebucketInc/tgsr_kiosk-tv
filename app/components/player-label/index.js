// imports
import React from 'react'
import FadeInWrapper from '../fadein-wrapper'

// component
import { IndextoGenre } from '../../utils'

// styles
import './styles.global.scss'

// component
const PlayerLabel = ({
    isPlaying,
    isSpinning,
    selection,
    isReady,
    onLearnMore,
}) => {
    return (
        <React.Fragment>
            {/* Currently Playing */}
            <div className="label-container">
                {/* Title */}
                {!isSpinning ? (
                    <p className="title">
                        {isPlaying ? 'Now playing' : 'Play to start listening'}
                    </p>
                ) : (
                    <p className="title">Switching to</p>
                )}
                <img
                    src={require('./assets/label.png')}
                    alt="BoomBox"
                    className="img-fluid inactive"
                />
                <FadeInWrapper isVisible={isPlaying}>
                    <img
                        src={require('./assets/label_active.png')}
                        alt="BoomBox"
                        className="img-fluid active animated flash"
                    />
                </FadeInWrapper>
                <FadeInWrapper
                    isVisible={(isReady & !isPlaying) === 0 ? false : true}>
                    <img
                        src={require('./assets/label_active.png')}
                        alt="BoomBox"
                        className="img-fluid active animated flash-infinite"
                    />
                </FadeInWrapper>
                <p className="selection">
                    <span>{IndextoGenre(selection.left, 'left')}</span>
                    <span>{IndextoGenre(selection.right, 'right')}</span>
                </p>
                <div
                    className="marquee-container"
                    onClick={() => onLearnMore(true)}>
                    <div className="label a">
                        <img
                            className="invisible"
                            src={require('./assets/marquee-x.png')}
                            alt="learn more"
                        />
                        <p>Click here to learn more about the mashup</p>
                        <img
                            src={require('./assets/marquee-x.png')}
                            alt="learn more"
                        />
                    </div>
                    <div className="label b">
                        <img
                            className="invisible"
                            src={require('./assets/marquee-x.png')}
                            alt="learn more"
                        />
                        <p>Click here to learn more about the mashup</p>
                        <img
                            src={require('./assets/marquee-x.png')}
                            alt="learn more"
                        />
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

// export
export default PlayerLabel
