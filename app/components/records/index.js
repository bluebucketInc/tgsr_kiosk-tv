// imports
import React, { Component } from 'react'
import { Draggable } from 'gsap/all'
import ThrowPropsPlugin from '../../assets/lib/ThrowPropsPlugin.js'

// components
import FadeInWrapper from '../fadein-wrapper'
import { NearestNo, SetRecord } from '../../utils'
import { leftRecordPoints, rightRecordPoints } from '../../utils/settings'

// styles
import './styles.global.scss'

// plugin to disable treeshacking
const plugin = ThrowPropsPlugin

// component
export default class Records extends Component {
    // on mount
    componentDidMount() {
        // left record
        const leftDraggable = Draggable.create('#leftRecord', {
            type: 'rotation',
            throwProps: true,
            onDragStart: this.props.onSpinStart,
            onThrowComplete: this.props.onSpinEnd,
            snap: endValue => {
                // final
                let returnValue
                let selection
                // calculate selection
                let negative = endValue < 0
                // closest angle to end value
                let closest = NearestNo(leftRecordPoints)(
                    Math.abs(endValue) % 360
                )
                // selected genre
                let pre_selected = Math.abs(5 - (360 - closest) / 72)
                if (pre_selected === 0) {
                    selection = negative ? 4 : 0
                } else {
                    selection = Math.abs(pre_selected + (negative ? -5 : 0))
                }
                // how many 360 inside endvalue
                let modulus = Math.floor(Math.abs(endValue) / 360)
                let baseAngle = 360 * modulus
                // return
                if (negative) {
                    returnValue = (baseAngle + (5 - selection) * 72) * -1
                } else {
                    returnValue = baseAngle + selection * 72
                }
                // set value
                this.props.onSelectLeft(selection)
                // snap
                return returnValue
            },
        })

        // right record
        const rightDraggable = Draggable.create('#rightRecord', {
            type: 'rotation',
            throwProps: true,
            onDragStart: this.props.onSpinStart,
            onThrowComplete: this.props.onSpinEnd,
            snap: endValue => {
                // final
                let returnValue
                let selection
                // calculate selection
                let negative = endValue < 0
                // closest angle to end value
                let closest = NearestNo(rightRecordPoints)(
                    Math.abs(endValue) % 360
                )
                // selected genre
                let pre_selected = Math.abs(5 - (360 - closest) / 72)
                if (pre_selected === 0) {
                    selection = negative ? 4 : 0
                } else {
                    selection = Math.abs(pre_selected + (negative ? -5 : 0))
                }
                // how many 360 inside endvalue
                let modulus = Math.floor(Math.abs(endValue) / 360)
                let baseAngle = 360 * modulus

                // return
                if (negative) {
                    returnValue = (baseAngle + (5 - selection) * 72) * -1
                } else {
                    returnValue = baseAngle + selection * 72
                }
                // console.table([
                //     ['endValue', endValue],
                //     ['selection', selection],
                //     ['returnValue', returnValue],
                //     ['modulus', modulus],
                //     ['angleBase', baseAngle],
                //     ['closest', closest],
                // ])
                // set value
                this.props.onSelectRight(selection)
                // snap
                return returnValue
            },
        })


        // // run initial animations
        // if (this.props.isInitialLoad) {
        //     setTimeout(() => {
        //         this.props.onRandom()
        //     }, 1000)
        // } else {
        //     // if not rotate to previous state
        //     SetRecord('left', this.props.selection.left)
        //     SetRecord('right', this.props.selection.right)
        // }
    }

    // render method
    render() {
        return (
            <React.Fragment>
                {/*record container*/}
                <div id="leftRecord" className="record-container left">
                    <FadeInWrapper
                        classes="glow-container"
                        isVisible={
                            this.props.isPlaying || this.props.isInitialLoad
                        }>
                        <img
                            className="animated glow flash-infinite"
                            src={require('./assets/record_left_glow.png')}
                            alt="Record"
                        />
                    </FadeInWrapper>
                    <img
                        className="record"
                        src={require('./assets/record_left.png')}
                        alt="Record"
                    />
                    <img
                        className="genre"
                        src={require('./assets/genre-left.png')}
                        alt="Record"
                    />
                    <FadeInWrapper
                        classes="glow-container"
                        isVisible={!this.props.isSpinning}>
                        <img
                            className="genre"
                            src={require(`./assets/glow/left/${this.props.selection.left}.png`)}
                            alt="Record"
                        />
                    </FadeInWrapper>
                </div>
                <div id="rightRecord" className="record-container right">
                    <FadeInWrapper
                        classes="glow-container"
                        isVisible={
                            this.props.isPlaying || this.props.isInitialLoad
                        }>
                        <img
                            className="animated glow flash-infinite"
                            src={require('./assets/record_right_glow.png')}
                            alt="Record"
                        />
                    </FadeInWrapper>
                    <img
                        className="record"
                        src={require('./assets/record_right.png')}
                        alt="Record"
                    />
                    <img
                        className="genre"
                        src={require('./assets/genre-right.png')}
                        alt="Record"
                    />
                    <FadeInWrapper
                        classes="glow-container"
                        isVisible={!this.props.isSpinning}>
                        <img
                            className="genre"
                            src={require(`./assets/glow/right/${this.props.selection.right}.png`)}
                            alt="Record"
                        />
                    </FadeInWrapper>
                </div>
            </React.Fragment>
        )
    }
}
