// m.design
import { makeStyles, createMuiTheme } from '@material-ui/core/styles'

// custom styles
export const formStyles = makeStyles(theme => ({
    textinput: {
        borderRadius: 0,
        marginTop: 0,
        border: '1px solid #cdd7e3',
    },
}))

// theme
export const theme = createMuiTheme({
    typography: {
        fontSize: 16,
        fontFamily: 'Din-2014',
        fontWeightLight: 200,
        fontWeightRegular: 200,
        fontWeightMedium: 200,
        fontWeightBold: 200,
    },
    palette: {
        primary: {
            main: '#0e469b',
            contrastText: '#ffffff',
        },
        secondary: {
            main: '#00a3e0',
            contrastText: '#ffffff',
        },
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                '@font-face': 'din-pro',
            },
        },
    },
})
