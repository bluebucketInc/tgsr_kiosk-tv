/*global window*/

// imports
import { api } from '../utils/settings'
import { getSweepsProbability, socialShare, IndextoGenre } from '../utils'
import store from '../state/store'

// action types
const ON_VOTE_CLOSE = 'vote/ON_VOTE_CLOSE'
const ON_NEXT_STEP = 'vote/ON_NEXT_STEP'
const ON_CAPTCHA_COMPLETE = 'vote/ON_CAPTCHA_COMPLETE'
const ON_FORM_SUBMIT = 'vote/ON_FORM_SUBMIT'
const ON_ERROR = 'vote/ON_ERROR'

// initial state
const initialState = {
    currentStep: 1,
    chances: getSweepsProbability(),
    isCaptchaVerified: false,
    isError: false,
}

// reducer //
export default (state = initialState, action) => {
    switch (action.type) {
        case ON_VOTE_CLOSE:
            return {
                ...state,
                currentStep: 1,
            }

        case ON_NEXT_STEP:
            return {
                ...state,
                currentStep: action.payload,
            }

        case ON_CAPTCHA_COMPLETE:
            return {
                ...state,
                isCaptchaVerified: action.payload,
            }
        case ON_ERROR:
            return {
                ...state,
                isError: action.payload,
            }

        default:
            return state
    }
}

// actions //

// on start vote
export const onVoteSubmit = (left, right) => {
    // set state
    return dispatch => {
        // go to next step
        dispatch(onNextStep(2))
        // send event to datalayer
        // window.dataLayer.push({
        //     vote:
        //         IndextoGenre(left, 'left') + '-' + IndextoGenre(right, 'right')+' ipadKiosk',
        // })
    }
}

// on start vote
export const onVoteOverlayClose = payload => {
    // set state
    return dispatch => {
        dispatch({
            type: ON_ERROR,
            payload: false,
        })
        dispatch({
            type: ON_VOTE_CLOSE,
            payload,
        })
    }
}

// onboarding complete
export const onNextStep = payload => {
    // set state
    return dispatch => {
        dispatch({
            type: ON_NEXT_STEP,
            payload,
        })
    }
}

// on share
export const onShare = type => {
    // selection
    const selection = store.getState().player.selection

    // share function
    socialShare(
        `${IndextoGenre(selection.left, 'left')}x${IndextoGenre(
            selection.right,
            'right'
        )}`,
        type
    )

    // go to next step
    return dispatch => {
        dispatch(onNextStep(3))
    }
}

// on onCaptchaComplete
export const onCaptchaComplete = key => {
    // go to next step
    return dispatch => {
        // submit form
        // axios
        //     .post(api.gateway + api.captcha, {
        //         'g-recaptcha-response': key,
        //     })
        //     .then(res => {
        //         dispatch({
        //             type: ON_CAPTCHA_COMPLETE,
        //             payload: true,
        //         })
        //     })
        //     .catch(err => {
        //         dispatch({
        //             type: ON_ERROR,
        //             payload: true,
        //         })
        //     })
    }
}

// on form submit
export const onFormSubmit = values => {
    // go to next step
    return dispatch => {
        // submit form
        axios
            .post(api.gateway + api.save, {
                userData: {
                    ...values,
                },
                no_of_hits: store.getState().vote.chances,
            })
            .then(res => {
                dispatch(onNextStep(5))
            })
            .catch(err => {
                dispatch({
                    type: ON_ERROR,
                    payload: true,
                })
            })
    }
}
