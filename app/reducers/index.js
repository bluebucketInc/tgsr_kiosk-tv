// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import player from './player';
import vote from './vote';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    player,
    vote
  });
}
