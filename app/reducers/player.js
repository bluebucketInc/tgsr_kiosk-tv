// imports
import store from '../state/store'
import {
    setRandomGenre,
    resetVolume,
    AnimateRecord,
    playSpinSound,
} from '../utils'
import {
    randomPlayDelay,
    leftRecordPoints,
    rightRecordPoints,
} from '../utils/settings'
import { mashups } from '../data/mashup-songs'

// action types
const ON_READY = 'player/ON_READY'
const ON_PLAY = 'player/ON_PLAY'
const ON_PAUSE = 'player/ON_PAUSE'
const ON_START_SPIN = 'player/ON_START_SPIN'
const ON_END_SPIN = 'player/ON_END_SPIN'
const ON_SELECT_LEFT = 'player/ON_SELECT_LEFT'
const ON_SELECT_RIGHT = 'player/ON_SELECT_RIGHT'
const ON_INIT_ANIM_COMPLETE = 'player/ON_INIT_ANIM_COMPLETE'
const ON_ONBOARDING_COMPLETE = 'player/ON_ONBOARDING_COMPLETE'
const ON_MASHUP_OVERLAY = 'player/ON_MASHUP_OVERLAY'
const ON_BUTTON_BUSY = 'player/ON_BUTTON_BUSY'

// initial state
const initialState = {
    hasOnboarded: false,
    isInitialLoad: true,
    isReady: false,
    isSpinning: false,
    isPlaying: false,
    isButtonBusy: false,
    selection: {
        left: 0,
        right: 0,
        mashup: '00',
        hasPlayed: false,
    },
    popup: {
        mashupOverlay: false,
        whyVote: false,
        winPrize: false,
    },
}

// reducer //
export default function player(state = initialState, action){
    switch (action.type) {
        case ON_READY:
            return {
                ...state,
                isReady: action.payload,
            }

        case ON_ONBOARDING_COMPLETE:
            return {
                ...state,
                hasOnboarded: true,
            }

        case ON_PLAY:
            return {
                ...state,
                isPlaying: true,
                isReady: false,
                selection: {
                    ...state.selection,
                    hasPlayed: true,
                },
            }

        case ON_PAUSE:
            return {
                ...state,
                isPlaying: false,
            }

        case ON_START_SPIN:
            return {
                ...state,
                isSpinning: true,
            }

        case ON_END_SPIN:
            return {
                ...state,
                isSpinning: false,
            }

        case ON_BUTTON_BUSY:
            return {
                ...state,
                isButtonBusy: action.payload,
            }

        case ON_SELECT_LEFT:
            return {
                ...state,
                selection: {
                    ...state.selection,
                    hasPlayed: false,
                    left: action.payload,
                    mashup: '' + action.payload + state.selection.right,
                },
            }

        case ON_SELECT_RIGHT:
            return {
                ...state,
                selection: {
                    ...state.selection,
                    hasPlayed: false,
                    right: action.payload,
                    mashup: '' + state.selection.left + action.payload,
                },
            }

        case ON_INIT_ANIM_COMPLETE:
            return {
                ...state,
                isInitialLoad: false,
                isReady: true,
            }

        case ON_MASHUP_OVERLAY:
            return {
                ...state,
                popup: {
                    ...state.popup,
                    mashupOverlay: action.payload,
                },
            }

        default:
            return state
    }
}

// actions //

// onboarding complete
export const setButtonBusy = payload => {
    // set state
    return dispatch => {
        dispatch({
            type: ON_BUTTON_BUSY,
            payload,
        })
    }
}

// onboarding complete
export const onOnboardingComplete = () => {
    // set state
    return dispatch => {
        dispatch({
            type: ON_ONBOARDING_COMPLETE,
        })
    }
}

// play
export const onPlay = () => {
    // set state
    return dispatch => {
        dispatch({
            type: ON_PLAY,
        })

        // player
        let player = mashups[store.getState().player.selection.mashup].player
        // play song
        player.play()
        player.on('end', () => dispatch(onPause()))
    }
}

// pause
export const onPause = () => {
    // player
    let player = mashups[store.getState().player.selection.mashup].player

    // pause song
    player.fade(1, 0, 500)
    player.on('fade', () => {
        player.stop()
        resetVolume(player)
    })

    // set state
    return dispatch =>
        dispatch({
            type: ON_PAUSE,
        })
}

// spin start
export const onSpinStart = () => {
    return dispatch => {
        // spin action
        dispatch({
            type: ON_START_SPIN,
        })
        // pause music
        dispatch(onPause())
    }
}

// spin end
export const onSpinEnd = () => {
    return dispatch =>
        dispatch({
            type: ON_END_SPIN,
        })
}

// spin end
export const onSelectLeft = payload => {
    return dispatch =>
        dispatch({
            type: ON_SELECT_LEFT,
            payload,
        })
}

// select right
export const onSelectRight = payload => {
    return dispatch =>
        dispatch({
            type: ON_SELECT_RIGHT,
            payload,
        })
}

// on popup overlay
export const onMashupOverlay = payload => {
    return dispatch =>
        dispatch({
            type: ON_MASHUP_OVERLAY,
            payload,
        })
}

// on onGenreClick
export const onGenreClick = (side, index) => {
    return dispatch => {
        // disable button
        dispatch(setButtonBusy(true))
        // play spin sound
        playSpinSound()
        // on spin star
        dispatch(onPause())
        // animate
        AnimateRecord(
            side,
            side === 'left' ? leftRecordPoints[index] : rightRecordPoints[index]
        )
        // set state
        if (side === 'left') {
            dispatch(onSelectLeft(index))
        } else {
            dispatch(onSelectRight(index))
        }

        // play after 2 seconds
        setTimeout(() => {
            const { isPlaying } = store.getState().player
            if (!isPlaying) {
                dispatch(onPlay())
            }
            dispatch(setButtonBusy(false))
        }, randomPlayDelay)
    }
}

// onPlayRandom
export const onPlayRandom = payload => {
    // set state
    return dispatch => {
        // disable button
        dispatch(setButtonBusy(true))
        // pause
        dispatch(onPause())
        // set random
        const { selection } = store.getState().player
        const { left, right } = setRandomGenre(selection.left, selection.right)
        // set selections
        dispatch(onSelectLeft(left))
        dispatch(onSelectRight(right))
        // play after 2 seconds
        setTimeout(() => {
            if (!store.getState().player.isPlaying) {
                dispatch(onPlay())
            }
            dispatch(setButtonBusy(false))
        }, randomPlayDelay)
    }
}

// onInitialSpin
export const onInitRandom = payload => {
    // set random
    const selection = store.getState().player.selection
    const { left, right } = setRandomGenre(selection.left, selection.right)

    // set state
    return dispatch => {
        // set selections
        dispatch(onSelectLeft(left))
        dispatch(onSelectRight(right))
        dispatch({
            type: ON_INIT_ANIM_COMPLETE,
        })
    }
}
