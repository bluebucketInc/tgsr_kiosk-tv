
// About Alumini
AOS.init();
 // Accordion Component 
 $("#myInput").on("keyup", function() {
     console.log('checking');
    var value = $(this).val().toLowerCase();
    $("#accordion1 .card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
(function ($) {

    var _$dropdown, _$listItem, _accordianList;

    function _dropdownHandler(e) {
        e.stopPropagation();

        $(e.currentTarget).toggleClass("active");
        $(e.currentTarget).next().toggleClass("active");
    }

    function _listItemClickHandler(e) {
        e.preventDefault();
        var $item = $(e.currentTarget);
        _$listItem.removeClass("active");
        $item.addClass("active");

        //for responsive dropdown widget...
        $item.closest("ul").removeClass("active");
        _$dropdown.removeClass("active");
        _$dropdown.find("span").html($item.text());

        _hashChangeHandler($item.attr("href"));
    }

    function _hashChangeHandler(location) {
        location = location.replace(/\#/g, "");
        if (location) {
            _accordianList.hide().filter(function (i, item) {
                return $(item).attr("id").indexOf(location) > -1;
            }).fadeIn();
        }
    }

    function _variables() {
        _$dropdown = $(".dropdown");
        _$listItem = $(".faq-list li a");
        _accordianList = $(".accordion.list");

        $.fn.randomize = function (selector) {
            var $elems = selector ? $(this).find(selector) : $(this).children(),
                $parents = $elems.parent();

            $parents.each(function () {
                $(this).children(selector).sort(function () {
                    return Math.round(Math.random()) - 0.5;
                }).detach().appendTo(this);
            });

            return this;
        };
    }

    function _addEvents() {
        _$dropdown.on("click", _dropdownHandler);
        _$listItem.on("click", _listItemClickHandler);
        //$(window).on("click", _hashHandler);
    }

    function _hashHandler() {
        if (event.target.getAttribute('href')) {
            var location = event.target.getAttribute('href').replace(/\#/g, "");
            _hashChangeHandler(location);
        }
    }

    function _init() {
        _variables();
        _addEvents();
    }

    $(_init());
})(jQuery);

// Header JS
(function(){
    function _menuHandler(){
        $('header').toggleClass('active');
        $('.body-contents').toggleClass('content-toggle');
        $('body').toggleClass('background-toggle');
        $('.tgsr-logo').toggleClass('menu-triggered');
        $(['.music-icon','.share-icon']).toggleClass('hide');
        // $('.spot-lights').toggleClass('menu-triggering');
    }

    function _addEvents(){
        $('.menu-icon').click(_menuHandler)
    }
    function _toggleSound(){
            var myAudio = document.getElementById('notifypop');
            if(!$('.music-icon').hasClass('paused')) {
               myAudio.play();
               myAudio.loop = true; 
            }
              else {
                myAudio.pause();
                myAudio.loop = true; 
            }
    }
    $('.music-icon').click(function(){
       _toggleSound();
    })

    function _init(){
        _addEvents();
    }
    $(_init());
})(jQuery);


// Social Share Component 
const links = document.querySelectorAll('.social-share-link a');

function onClick(event) {
    event.preventDefault();
    window.open(
    event.currentTarget.href,
        'Поделиться',
        'width=600,height=500,location=no,menubar=no,toolbar=no'
    );
}

$.each(links, function(i, link){
    const url = encodeURIComponent(window.location.origin + window.location.pathname);
    // const title = encodeURIComponent(document.title);

    link.href = link.href.
    replace('{url}', url);
    // replace('{title}', title);

    link.addEventListener('click', onClick);
});




// (function(d, s, id) {
//     var js, fjs = d.getElementsByTagName(s)[0];
//     if (d.getElementById(id)) return;
//     js = d.createElement(s); js.id = id;
//     js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
//     fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));



(function ($) {

    let _homeCarousel;


    function _slickInitHandler(e) {
        _homeCarousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        if(currentSlide < nextSlide){
            $('.tgsr-logo').removeClass('home-carousel-active');
        } else if(currentSlide == nextSlide && nextSlide == 1) {
            $('.tgsr-logo').removeClass('home-carousel-active');
        } else{
            $('.tgsr-logo').addClass('home-carousel-active');
        }

    }

    function _addEvents() {

        _homeCarousel.on("init", _slickInitHandler);
        _homeCarousel.on('beforeChange', _slickBeforeChangeHandler);
        // $(document).on("click", "video", function(e){
        //     e.preventDefault();
        //     return false;
        // })
        
        $(document).on("click", ".music-icon", function (e) {
            $(e.currentTarget).toggleClass("paused");
        })
    }

    function _initializeCarousal() {
        _homeCarousel.slick({
            // centerMode: true,
            // centerPadding: '60px',
            dots: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function _variablesInitialize() {
        _homeCarousel = $('.home-slider');

        if(location.pathname == "/" || location.pathname.toLocaleLowerCase() == "/index.html"){

            $(".tgsr-logo.home-carousel-active").find("img").attr("src", "/clientlibs/images/global/tgsr_logo.gif");
        } else {
            $(".tgsr-logo.home-carousel-active").removeClass("home-carousel-active")
        }
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
    }

    $(_init());
})(jQuery);


(function ($) {

    let _mentorCarousel, _mentorPopover;


    function _slickInitHandler(e) {
        _mentorCarousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        const slideCount = slick.slideCount - 1;
        const nextSlideIsFirst = currentSlide === slideCount;
        const nextSlideIsLast = nextSlide === slideCount;
        const $activeSlide = $('.slick-slide.slick-current');
        if (nextSlideIsFirst && nextSlide == 0) {
            $activeSlide.next('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.next('.slick-slide').next(".slick-slide").attr("data-aria-hidden", "false");
        } else if (nextSlideIsLast && currentSlide == 0) {
            $activeSlide.prev('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.prev('.slick-slide').prev(".slick-slide").attr("data-aria-hidden", "false");
        }

        _mentorCarousel.find('[aria-hidden="false"]').attr("data-aria-hidden", "false");
    }

    function _slickAfterChangeHandler(e, slick, currentSlide, nextSlide) {
        $('.slick-clone-current').removeClass('slick-clone-current');
        _mentorCarousel.find('[data-aria-hidden="false"]').attr("data-aria-hidden", "");
    }

    function _hashChangeHandler(location) {
        _mentorPopover.hide();
        if (location) {
            $("html, body").animate({scrollTop: 0}, 500);
            var $mentorSlides = _mentorPopover.find(".mentor-slide").hide();

            $mentorSlides.filter(function (i, mentorSlideElement) {
                return $(mentorSlideElement).attr("data-id") == location;
            }).show(100, function(){
                _mentorPopover.fadeIn();
            });
        }
    }

    function _closeOverlayHandler(e) {
        $(".popover").fadeOut();
    }

    function _addEvents() {

        _mentorCarousel.on("init", _slickInitHandler);
        _mentorCarousel.on('beforeChange', _slickBeforeChangeHandler);
        _mentorCarousel.on('afterChange', _slickAfterChangeHandler);

        $(document).on("click", "[overlay-dialog]", function (e) {
            e.preventDefault();
            var href = $(e.currentTarget).attr("href");
            if (href) {
                _hashChangeHandler(href)
            }
        });

        $(document).on("click", ".close-overlay", _closeOverlayHandler)
    }

    function _initializeCarousal() {
        _mentorCarousel.randomize(".mentor-item");
        _mentorCarousel.slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            dots: true,
            speed: 1000,
            draggable: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function _variablesInitialize() {
        _mentorCarousel = $('.mentors-carousel');
        _mentorPopover = $(".mentor-detail.popover");
    }

    function _indentText() {
        $('.mentor-slide:nth-child(4) p:nth-child(n+5)').css({'margin-left': '25px', 'display': 'list-item'})
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
        //_indentText();
    }


    $(_init());
})(jQuery);

(function ($) {
    var _sections, _nav, _nav_height, _navbar, _sticky, _formValidation, _isConsentRequired = false;
 
    function _navigationHandler() {
        var $el = $(this)
            , id = $el.attr('href');
 
        $('html, body').animate({
            scrollTop: $(id).offset().top - _nav_height
        }, 500);
 
        return false;
    }
 
    function _windowScrollHandler() {
        var cur_pos = $(this).scrollTop();
 
        _sections.each(function () {
            var top = $(this).offset().top - _nav_height,
                bottom = top + $(this).outerHeight();
 
            if (cur_pos >= top && cur_pos <= bottom) {
                _nav.find('a').removeClass('active');
                _sections.removeClass('active');
 
                $(this).addClass('active');
                _nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
            }
        });
 
        if (window.pageYOffset >= _sticky) {
            _navbar? _navbar.classList.add("_sticky") : null;
        } else {
            _navbar? _navbar.classList.remove("_sticky") : null;
        }
    }
   
    function _signUpResetHandler(e) {
        $(e.currentTarget).find("button[type='submit']").prop("disabled", true).closest(".expanding-btn").addClass("disabled");
    }
 
    function _signUpHandler(e) {
        e.preventDefault();
        var formData = {};
        $(e.currentTarget).serializeArray().map(function (item) {
            formData[item.name] = item.value;
        });
 
        try{
            $.ajax("/api/create", {
                type: "post",
                dataType: "json",
                data: formData,
                success: function (e) {
                    $("#sucess-msg").fadeIn();
                },
                error: function (e) {
                    $("#error-msg").fadeIn();
                },
                complete: function (e) {
                    $("#sign-up").hide();
                }
            })
        } catch (e) {
            $("#signup").hide();
            $("#error-msg").fadeIn();
        }
 
 
    }
 
    function _nameValidation($this) {
        _formValidation.data.name = /^[a-zA-Z]+([ ]?[a-zA-Z])*$/g.test($this.val());
        return _formValidation.data.name ? "valid" : "invalid";
    }
 
    function _emailValidation($this) {
        _formValidation.data.email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g.test($this.val());
        return _formValidation.data.email ? "valid" : "invalid";
    }
 
    function _contactNumberValidation($this) {
        _formValidation.data.contantNumber = /^(\d{8})$/g.test($this.val());
        return _formValidation.data.contantNumber ? "valid" : "invalid";
    }
 
    function _artistNameValidation($this) {
        _formValidation.data.artistName = true;
        return _formValidation.data.artistName ? "valid" : "invalid";
    }
 
    function _ageValidation($this) {
        _formValidation.data.age = /^(\d{2,3})$/g.test($this.val()) && parseInt($this.val()) >= 15;
        _isConsentRequired = false;
        _formValidation.data.consent = false;
 
        var $consent = $("#styled-checkbox-3").closest(".form-group.checkboxes").hide();
 
        if(_formValidation.data.age && parseInt($this.val()) < 18){
            _isConsentRequired = true;
            $consent.fadeIn();
        }
        return _formValidation.data.age ? "valid" : "invalid";
    }
 
    function _webVideoLinkValidation($this) {
        _formValidation.data.webVideoLink = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/g.test($this.val());
        return _formValidation.data.webVideoLink ? "valid" : "invalid";
    }
 
    function _termsValidation($this) {
        _formValidation.data.termsValidation = $this.prop("checked");
 
        // if(_formValidation.data.termsValidation){
        //     window.open("clientlibs/documents/tgsr_tnc.pdf", "_blank");
        // }
 
        return _formValidation.data.termsValidation ? "valid" : "invalid";
    }
 
    function _agreeValidation($this) {
        _formValidation.data.agreeValidation = $this.prop("checked");
        return _formValidation.data.agreeValidation ? "valid" : "invalid";
    }
 
    function _consentValidation($this) {
        _formValidation.data.consent = $this.prop("checked");
        return _formValidation.data.consent ? "valid" : "invalid";
    }
 
    function _checkSubmitButton() {
        var result = true;
        var resultShouldbe = {
            age: true,
            agreeValidation: true,
            artistName: true,
            consent: _isConsentRequired,
            contantNumber: true,
            email: true,
            name: true,
            termsValidation: true,
            webVideoLink: true,
        };
        for (var item in resultShouldbe) {
            if(_formValidation.data[item] != resultShouldbe[item]){
                result = false;
            }
        }
 
        var isDisabled = !result;
 
        $("#sign-up button[type='submit']").prop("disabled", isDisabled);
        $("#sign-up button[type='submit']").closest(".expanding-btn")[isDisabled ? "addClass" : "removeClass"]("disabled");
    }
 
    function _checkForValidationHandler(e) {
        var $this = $(this);
        var fn = $this.attr("name") + "Validation";
        $this.closest(".form-group").removeClass("valid invalid").addClass(_formValidation[fn]($($this)));
        _checkSubmitButton();
    }
 
    function _addEvents() {
        $(window).on('scroll', function () {
            var cur_pos = $(this).scrollTop();
 
            _sections.each(function() {
                var top = $(this).offset().top - _nav_height,
                    bottom = top + $(this).outerHeight();
 
                if (cur_pos >= top && cur_pos <= bottom) {
                    _nav.find('a').removeClass('active');
                    _sections.removeClass('active');
 
                    $(this).addClass('active');
                    _nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
                }
            });
 
            if (window.pageYOffset >= _sticky) {
                _navbar? _navbar.classList.add("sticky"): null;
            } else {
                _navbar? _navbar.classList.remove("sticky"): null;
            }
        });
 
        _nav.find('a').on('click', _scrollToSection);
        $("#section-1 .sign-up-button").find('a').on('click', _scrollToSection);

        $("form#sign-up").on("submit", _signUpHandler);
        $("form#sign-up").on("reset", _signUpResetHandler);
        $("form#sign-up").on("input change", ":input", _checkForValidationHandler);
    }
    function _scrollToSection() {
        var $el = $(this)
            , id = $el.attr('href');

        $('html, body').animate({
            scrollTop: $(id).offset().top - _nav_height
        }, 500);

        return false;
    }
 
    function _variables() {
        _sections = $('section');
        _nav = $('.sticky-nav');
        _nav_height = _nav.outerHeight();
 
        _navbar = document.getElementById("navigation-section");
        _navbar?_sticky = _navbar.offsetTop:null;
 
        _formValidation = {
            data: {
                artistName: true,
                consent: false
            },
            nameValidation: _nameValidation,
            emailValidation: _emailValidation,
            contactNumberValidation: _contactNumberValidation,
            ageValidation: _ageValidation,
            artistNameValidation: _artistNameValidation,
            webVideoLinkValidation: _webVideoLinkValidation,
            termsValidation: _termsValidation,
            agreeValidation: _agreeValidation,
            consentValidation: _consentValidation
        }
    }
 
    function _getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
 
    function _randomizeMentorImages(){
        var arrImgs = ["/clientlibs/images/open-call/img_opencall_point01_charlie@2x.png", "/clientlibs/images/open-call/img_opencall_point01_joanna@2x.png", "/clientlibs/images/open-call/img_opencall_point01_kelly@2x.png", "/clientlibs/images/open-call/img_opencall_point01_sezairi@2x.png", "/clientlibs/images/open-call/img_opencall_point01_shabir@2x.png"];
        var randomIndex = _getRandomInt(arrImgs.length);
 
        var $imgElement = $("#section-2 .stage-background").find("img");
        $imgElement.attr("src", arrImgs[randomIndex]);
 
    }

    function _scrollSave(x){
        $('html, body').animate({
            scrollTop: $(x).offset().top 
        }, 60);
    }
 
    function _init() {
        _variables();
        _addEvents();
        _randomizeMentorImages()
    }

    $('.tnc').click(function(e){
        e.preventDefault();
        window.open("clientlibs/documents/tgsr_opencall_tnc.pdf","target=_blank");
        _scrollSave('.tnc')
        return false;
    });
    $('.consent-form').click(function(e){
        e.preventDefault();
        window.open("clientlibs/documents/tgsr_consentform.pdf", "target=_blank");
        _scrollSave('.consent-form')
        return false;
    })
 
    $(_init())
})(jQuery);
 
 
 
  //FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal() {
var trigger = $("body").find('[data-toggle="modal"]');
      trigger.click(function () {
          var theModal = $(this).data("target"),
              videoSRC = $(this).attr("data-theVideo"),
              videoSRCauto = videoSRC;
           
          $(theModal + ' iframe').attr('src', videoSRCauto);
          $(theModal + ' button.close').click(function () {
              $(theModal + ' iframe').attr('src', videoSRC);
          });
          $('.close-video-btn').click(function(){
            $(theModal + ' iframe').attr('src', 'blank');
            $('.nav-bar').css({'opacity':'1','z-index':'99'});
            $('.absolute-header').css('z-index',999);
         });
          setTimeout(function(){
            $('.modal-backdrop').hide();
            $('.nav-bar').css({'opacity':'.1'});
            $('.absolute-header').css('z-index',1);
        },100);
      });
  }
 
  autoPlayYouTubeModal();

(function ($) {

    let _$carousel, _mentorPopover, _selectedMode;

    function _slickInitHandler(e) {
        _$carousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        const slideCount = slick.slideCount - 1;
        const nextSlideIsFirst = currentSlide === slideCount;
        const nextSlideIsLast = nextSlide === slideCount;
        const $activeSlide = $('.slick-slide.slick-current');
        if (nextSlideIsFirst && nextSlide == 0) {
            $activeSlide.next('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.next('.slick-slide').next(".slick-slide").attr("data-aria-hidden", "false");
        } else if (nextSlideIsLast && currentSlide == 0) {
            $activeSlide.prev('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.prev('.slick-slide').prev(".slick-slide").attr("data-aria-hidden", "false");
        }

        _$carousel.find('[aria-hidden="false"]').attr("data-aria-hidden", "false");
    }

    function _slickAfterChangeHandler(e, slick, currentSlide, nextSlide) {
        $('.slick-clone-current').removeClass('slick-clone-current');
        _$carousel.find('[data-aria-hidden="false"]').attr("data-aria-hidden", "");
    }

    function _toggleHandler(e) {
       _toggleState($(e.target).prop("checked") ? "tunes" : "times");
    }

    function _toggleState(selectedState) {
        var mode, $elements = $(".timeline-times-background, .timeline-tunes-background, .times-title, .tunes-title, .times-carousel, .tunes-carousel");

        if (selectedState.indexOf("#") > -1) {
            selectedState = selectedState.replace(/\#/g, "");
        }

        mode = selectedState.split('_')[0];

        if (mode && mode != _selectedMode) {

            $elements.removeClass("active").filter(function (i, element) {
                return $(element).attr("class").indexOf(mode) > -1 && $(element).attr("class").indexOf("carousel") == -1;
            }).addClass("active");

            _buildCarousal(mode);
        }

    }

    function _addEvents() {
        $(document).on("change", ".timeline-toggler input", _toggleHandler);
    }

    function _getSlickSettings(mode) {
        var settings = {
            "tunes": {
                centerMode: true,
                variableWidth: true,
                // centerPadding: '60px',
                speed: 1400,
                dots: true,
                slidesToShow: 1,
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            },
            "times": {
                centerMode: true,
                variableWidth: true,
                // centerPadding: '60px',
                speed: 1400,
                dots: true,
                adaptiveHeight: true,
                slidesToShow: 1,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            }
        };

        return settings[mode]? JSON.parse(JSON.stringify(settings[mode])): {};
    }

    function _initializeCarousal() {
        var mode = "times";
        if(location.hash){
            mode =  location.hash.replace(/\#/g, "").split("_")[0] == "tunes"? "tunes" : "times";
        }

        if(mode == "tunes" && !$(".timeline-toggler input").prop("checked")){
            $(".timeline-toggler input").prop("checked", true);
        }

        _toggleState(mode);
    }

    function _buildCarousal(mode){
        if(_$carousel && _$carousel.get(0)){
            _$carousel.hide();
            _$carousel.slick('unslick');
            _$carousel.removeClass("active");
            _$carousel.off("init", _slickInitHandler);
        }

        _selectedMode = mode;

        _$carousel = $("." + mode + "-carousel");
        _$carousel.on("init", _slickInitHandler);
        _$carousel.on('beforeChange', _slickBeforeChangeHandler);
        _$carousel.on('afterChange', _slickAfterChangeHandler);
        _$carousel.slick(_getSlickSettings(mode));
    }

    function _variablesInitialize() {

        _mentorPopover = $(".mentor-detail.popover");
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
    }

    $(_init());
})(jQuery);
