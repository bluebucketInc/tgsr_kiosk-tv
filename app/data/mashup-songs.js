// imports
import { Howl } from 'howler'
// settings
import { howlerOptions } from '../utils/settings'

// mashups
export const mashups = {
    '00': {
        player: new Howl({
            src: ['/assets/mashups/00.mp3'],
            ...howlerOptions,
        }),
    },
    '01': {
        player: new Howl({
            src: ['/assets/mashups/01.mp3'],
            ...howlerOptions,
        }),
    },
    '02': {
        player: new Howl({
            src: ['/assets/mashups/02.mp3'],
            ...howlerOptions,
        }),
    },
    '03': {
        player: new Howl({
            src: ['/assets/mashups/03.mp3'],
            ...howlerOptions,
        }),
    },
    '04': {
        player: new Howl({
            src: ['/assets/mashups/04.mp3'],
            ...howlerOptions,
        }),
    },
    '10': {
        player: new Howl({
            src: ['/assets/mashups/10.mp3'],
            ...howlerOptions,
        }),
    },
    '11': {
        player: new Howl({
            src: ['/assets/mashups/11.mp3'],
            ...howlerOptions,
        }),
    },
    '12': {
        player: new Howl({
            src: ['/assets/mashups/12.mp3'],
            ...howlerOptions,
        }),
    },
    '13': {
        player: new Howl({
            src: ['/assets/mashups/13.mp3'],
            ...howlerOptions,
        }),
    },
    '14': {
        player: new Howl({
            src: ['/assets/mashups/14.mp3'],
            ...howlerOptions,
        }),
    },
    '20': {
        player: new Howl({
            src: ['/assets/mashups/20.mp3'],
            ...howlerOptions,
        }),
    },
    '21': {
        player: new Howl({
            src: ['/assets/mashups/21.mp3'],
            ...howlerOptions,
        }),
    },
    '22': {
        player: new Howl({
            src: ['/assets/mashups/22.mp3'],
            ...howlerOptions,
        }),
    },
    '23': {
        player: new Howl({
            src: ['/assets/mashups/23.mp3'],
            ...howlerOptions,
        }),
    },
    '24': {
        player: new Howl({
            src: ['/assets/mashups/24.mp3'],
            ...howlerOptions,
        }),
    },
    '30': {
        player: new Howl({
            src: ['/assets/mashups/30.mp3'],
            ...howlerOptions,
        }),
    },
    '31': {
        player: new Howl({
            src: ['/assets/mashups/31.mp3'],
            ...howlerOptions,
        }),
    },
    '32': {
        player: new Howl({
            src: ['/assets/mashups/32.mp3'],
            ...howlerOptions,
        }),
    },
    '33': {
        player: new Howl({
            src: ['/assets/mashups/33.mp3'],
            ...howlerOptions,
        }),
    },
    '34': {
        player: new Howl({
            src: ['/assets/mashups/34.mp3'],
            ...howlerOptions,
        }),
    },
    '40': {
        player: new Howl({
            src: ['/assets/mashups/40.mp3'],
            ...howlerOptions,
        }),
    },
    '41': {
        player: new Howl({
            src: ['/assets/mashups/41.mp3'],
            ...howlerOptions,
        }),
    },
    '42': {
        player: new Howl({
            src: ['/assets/mashups/42.mp3'],
            ...howlerOptions,
        }),
    },
    '43': {
        player: new Howl({
            src: ['/assets/mashups/43.mp3'],
            ...howlerOptions,
        }),
    },
    '44': {
        player: new Howl({
            src: ['/assets/mashups/44.mp3'],
            ...howlerOptions,
        }),
    },
}
