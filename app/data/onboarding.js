// onboarding screens
export const onBoarding = [
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 1: PICK A TIME. PICK A TUNE.',
        image: '/assets/images/onboarding/one.png',
        image_d: '/assets/images/onboarding/one_desktop.png',
        firstSlide: true,
        video: {
            tablet: '/assets/videos/onboarding/1_tablet.mp4',
            desktop: '/assets/videos/onboarding/1_desktop.mp4',
            mobile: '/assets/videos/onboarding/mobile/1.mp4',
        },
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 2: LISTEN TO YOUR MASHUP',
        image: '/assets/images/onboarding/two.png',
        image_m: '/assets/images/onboarding/mobile/two.png',
        video: {
            desktop: '/assets/videos/onboarding/2.mp4',
            mobile: '/assets/videos/onboarding/mobile/2.mp4',
        },
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'STEP 3: VOTE FOR YOUR FAVOURITE MASHUP',
        image: '/assets/images/onboarding/three.png',
        image_m: '/assets/images/onboarding/mobile/three.png',
        video: {
            desktop: '/assets/videos/onboarding/3.mp4',
            mobile: '/assets/videos/onboarding/mobile/3.mp4',
        },
    },
    {
        sub: 'STAND A CHANCE TO WIN AMAZING PRIZES',
        head: `STEP 4: SUBMIT YOUR DETAILS AND ENTER THE DRAW`,
        image: '/assets/images/onboarding/four.png',
        image_m: '/assets/images/onboarding/mobile/four.png',
        end: true,
    },
]
