// imports
import React from 'react'
import { connect } from 'react-redux'

// modules
import ModelWrapper from '../../components/modal-wrapper'
import { overlay } from '../../data/mashup-overlay'
import { IndextoGenre } from '../../utils'

// styles
import './styles.global.scss'

// component
const MashupOverlay = ({ isActive, onTrigger, selection }) => {
    // content
    const content = overlay[selection.mashup]

    // render
    return (
        <React.Fragment>
            <ModelWrapper isActive={isActive} onClose={() => onTrigger(false)}>
                <div className="container modal-wrapper mashup-overlay">
                    <div className="row">
                        <div className="col-sm-12 text-center">
                            <div className="genre-wrapper mobile">
                                <h1>{IndextoGenre(selection.left, 'left')}</h1>
                                <img
                                    className="genre-x"
                                    src={require('./assets/close.png')}
                                    alt="Mashup"
                                />
                                <h1>
                                    {IndextoGenre(selection.right, 'right')}
                                </h1>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-6 image-wrapper">
                            <img
                                className="genre-image img-responsive"
                                src={content.image}
                                alt={content.mashup}
                            />
                        </div>
                        <div className="col-md-6 col-sm-6 text-wrapper">
                            <div className="content-wrapper">
                                <p>{content.para}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </ModelWrapper>
        </React.Fragment>
    )
}

// mapping state to props
const mapStateToProps = ({ player }) => ({
    selection: player.selection,
})

// export
export default connect(mapStateToProps)(MashupOverlay)
