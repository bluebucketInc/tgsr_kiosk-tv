// imports
import React, { useRef } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'

// modules
import { slickSettings } from '../../utils/settings'
import { onBoarding } from '../../data/onboarding'
import { Button } from '../../components/button'
import ModelWrapper from '../../components/modal-wrapper'

// styles
import './styles.global.scss'

// component
const OnboardingScreens = ({ onOnboardingComplete }) => {
    // reference
    const slider = useRef(null)
    // next slide
    const goNext = () => slider.current.slickNext()
    // render
    return (
        <React.Fragment>
            {/* Modla */}
            <ModelWrapper isActive={true} onClose={() => {}} to="/">
                <div
                    className={`onboarding-container`}>
                    {/* onboarding */}
                    <Slider
                        ref={slider}
                        {...slickSettings}
                        className="onboarding-slider">
                        {onBoarding.map(item => (
                            <div className="slides" key={item.sub}>
                                <React.Fragment>
                                    <div className="content-wrapper">
                                        <h5>{item.sub}</h5>
                                        <h3>{item.head}</h3>
                                    </div>
                                    <div className="media-wrapper">
                                        <img
                                            className="onboarding-media"
                                            src={item.image}
                                            alt={item.head}
                                        />
                                        {item.video && (
                                            <video
                                                className="video-container video-container-overlay"
                                                autoPlay={true}
                                                loop={true}
                                                muted={true}
                                                playsInline
                                                data-reactid=".0.1.0.0">
                                                <source
                                                    type="video/mp4"
                                                    data-reactid=".0.1.0.0.0"
                                                    src={
                                                        item.firstSlide
                                                            ? item.video
                                                                  .tablet
                                                            : item.video
                                                                  .desktop
                                                    }
                                                />
                                            </video>
                                        )}
                                    </div>
                                    {item.end ? (
                                        <Link
                                            to="/"
                                            onClick={onOnboardingComplete}>
                                            <Button classes="onboarding-btn long-btn">
                                                START SPINNING
                                            </Button>
                                        </Link>
                                    ) : (
                                        <Button
                                            classes="long-btn"
                                            onClick={goNext}>
                                            Next
                                        </Button>
                                    )}
                                </React.Fragment>
                            </div>
                        ))}
                    </Slider>
                </div>
            </ModelWrapper>
        </React.Fragment>
    )
}

// export
export default OnboardingScreens
