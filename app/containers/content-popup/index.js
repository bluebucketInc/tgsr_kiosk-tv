// imports
import React from 'react'
import { Button } from '../../components/button'
import { Link } from 'react-router-dom'

// modules
import ModelWrapper from '../../components/modal-wrapper'

// styles
import './styles.global.scss'

// why vote
export const WhyVote = () => {
    // render
    return (
        <ModelWrapper
            isActive={true}
            onClose={() => {}}
            to="/"
            className="why-vote-container">
            <div className="container overlay-container">
                <div className="youtube-container">
                    <iframe
                        title="TGSR"
                        className="youtube-frame"
                        width="740"
                        height="416"
                        src="https://www.youtube.com/embed/LQAvi28GjH8?rel=0"
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen={false}></iframe>
                    <img
                        src={require('./assets/youtube-bg.png')}
                        alt="Youtube"
                        className="bg-pattern"
                    />
                </div>
                <div className="content-container">
                    <h3>WHY SHOULD YOU VOTE FOR A UNIQUE MASHUP?</h3>
                    <p className="text-center">
                        You can help make music history. Your mashup will be the
                        inspiration source for the original "Made-in-Singapore"
                        songs created by TGSR’s aspiring local music talents.
                        You may also stand a chance to win amazing prizes by
                        voting your mashup.
                    </p>
                    <div className="inner-container">
                        <div className="row">
                            <div className="col-sm-6">
                                <h4 className="yellow-text">25 COMBINATIONS</h4>
                                <p>
                                    Choose from 25 unique combinations that are
                                    created by mashing up the distinct tunes
                                    from 5 iconic eras of Singapore’s history
                                    and 5 modern music genres.
                                </p>
                            </div>
                            <div className="col-sm-6">
                                <h4 className="yellow-text">
                                    10 ORIGINAL SONGS
                                </h4>
                                <p>
                                    The 10 most voted mashups will inspire the
                                    original "Made-inSingapore” songs that will
                                    be created by our aspiring music talents.
                                </p>
                            </div>
                            <div className="buttons-wrapper">
                                <Link to="/">
                                    <Button classes="long-btn">
                                        START SPINNING
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ModelWrapper>
    )
}

// why vote
export const WinPrizes = () => {
    // render
    return (
        <ModelWrapper isActive={true} onClose={() => {}} to="/">
            <div className="container prizes-container">
                <div className="row">
                    <div className="col-lg-6 col-md-12">
                        <img
                            className="prize-image"
                            src={require('./assets/prizes.png')}
                            alt="Want to win prizes?"
                        />
                    </div>
                    <div className="col-lg-6 col-md-12 text-left">
                        <h3 className="lightblue-text">
                            WIN AMAZING PRIZES EVERY WEEK!
                        </h3>
                        <p>Follow these simple steps and earn chances to win</p>
                        <div className="numbered-list">
                            <div>
                                <span className="bullet">1</span>
                                <span className="text">Create a mashup</span>
                            </div>
                            <div>
                                <span className="bullet">2</span>
                                <span className="text">
                                    Vote for your selected combination
                                </span>
                            </div>

                            <div>
                                <span className="bullet">3</span>
                                <span className="text">
                                    Unlock a card every time you vote for your
                                    mashup and enter the weekly draw.
                                    <br />
                                    <span className="small-text">
                                        The more you vote and submit your
                                        details to enter the weekly lucky draw,
                                        the higher your chances of winning! Each
                                        week, 3 lucky winners will be picked and
                                        we will contact them via the details
                                        provided.
                                    </span>
                                </span>
                            </div>
                            <div>
                                <span className="bullet">4</span>
                                <span className="text">
                                    Submit your details and we will get back to
                                    you if you are one of the lucky winners.
                                </span>
                            </div>
                            <div>
                                <span className="bullet">5</span>
                                <span className="text">
                                    Continue spinning and win more chances
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ModelWrapper>
    )
}
