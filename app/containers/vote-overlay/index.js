// imports
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// modules
import ModelWrapper from '../../components/modal-wrapper'
import {
    onVoteOverlayClose,
    onNextStep,
    onShare,
    onFormSubmit,
    onCaptchaComplete,
    onVoteSubmit,
} from '../../reducers/vote'
import {
    ReCaptchaPage,
    ThankYouKiosk,
    ErrorPage,
} from './components'

// styles
import './styles.global.scss'

// component
const VoteOverlay = ({
    selection,
    vote,
    onVoteOverlayClose,
    onCaptchaComplete,
    onNextStep,
    onShare,
    onFinalStep,
    onFormSubmit,
    onVoteSubmit,
}) => {
    // get current step
    const currentStep = () => {
        switch (vote.currentStep) {
            case 1:
                return (
                    <ReCaptchaPage
                        left={selection.left}
                        right={selection.right}
                        onVoteSubmit={onVoteSubmit}
                        isCaptchaVerified={vote.isCaptchaVerified}
                        onCaptchaComplete={onCaptchaComplete}
                    />
                )
            case 2:
                return (
                    <ThankYouKiosk
                        left={selection.left}
                        right={selection.right}
                        onClose={onVoteOverlayClose}
                    />
                )
            default:
                return <ErrorPage onClose={onVoteOverlayClose} />
        }
    }

    // render
    return (
        <ModelWrapper isActive={true} onClose={onVoteOverlayClose} to="/">
            <div
                className={`container vote-container`}>
                {vote.isError ? (
                    <ErrorPage onClose={onVoteOverlayClose} />
                ) : (
                    currentStep()
                )}
            </div>
        </ModelWrapper>
    )
}

// mapping state to props
const mapStateToProps = ({ player, vote }) => ({
    selection: player.selection,
    vote,
})

// dispatch to props
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            onVoteOverlayClose,
            onNextStep,
            onShare,
            onFormSubmit,
            onCaptchaComplete,
            onVoteSubmit,
        },
        dispatch
    )

// export
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VoteOverlay)
