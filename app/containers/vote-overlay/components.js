// imports
import React from 'react'
import { Link } from 'react-router-dom'
// components
import FadeInWrapper from '../../components/fadein-wrapper'
import { Button } from '../../components/button'
import { IndextoGenre } from '../../utils'

// styles
import './styles.global.scss'

// ReCaptcha
export const ReCaptchaPage = ({
    left,
    right,
    onVoteSubmit,
    onCaptchaComplete,
    isCaptchaVerified,
}) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>AWESOME CHOICE! YOU’RE VOTING FOR</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <Button
                classes="long-btn"
                onClick={() => onVoteSubmit(left, right)}>
                Submit
            </Button>
        </FadeInWrapper>
    )
}

// Social Share
export const SharePage = ({ left, right, onShare, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                Share your mashup to unlock your chances to win amazing prizes!
            </p>
            <p>
                <strong>
                    Should you win the weekly draw, you will be required to show
                    proof of posting at the point of prize collection.
                </strong>
            </p>
            <div className="button-group buttons-wrapper">
                <Button classes="long-btn" onClick={() => onShare('fb')}>
                    SHARE ON FACEBOOK
                </Button>
                <Button classes="long-btn" onClick={() => onShare('tw')}>
                    SHARE ON TWITTER
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-locked.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Draw Page
export const DrawPage = ({ left, right, onNextStep, onClose, chances }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>CONGRATULATIONS ON VOTING FOR YOUR FAVOURITE MASHUP</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You've unlocked {chances} chance({chances > 1 ? 's' : ''}) to
                win in this week's draw. Enter the draw redeem your chances!
            </p>
            <div className="buttons-wrapper">
                <Button classes="long-btn" onClick={() => onNextStep(4)}>
                    ENTER THE DRAW
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-unlocked_${chances}.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouKiosk = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You’ve voted for {IndextoGenre(left, 'left')} x{' '}
                {IndextoGenre(right, 'right')}. Simply approach any of our staff
                to collect
                <br />a scratch card and stand a chance to win amazing prizes!
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
            <div className="scratchcard-container">
                <img
                    src={require('./assets/scratch-card-locked.png')}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouDesktop = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">GOOD LUCK!</h3>
            <p>
                We will get in touch if you're one of the three lucky winners.
                <br />
                Meanwhile, why not create more mashups to get more chances
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}

// error page
export const ErrorPage = ({ onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">WHOOPS!</h3>
            <p>Something went wrong. Please try again later.</p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        Back to home
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}
