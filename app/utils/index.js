// imports
import { TweenMax, Expo } from 'gsap/TweenMax'
import { Howl } from 'howler'
import {
    genreList,
    leftRecordPoints,
    rightRecordPoints,
    randomSelectionSpeed,
    howlerOptions,
    recordAnimateSpeed,
    sweepChances,
    sharePage,
} from './settings'

// get random Integer
export const getRandomInt = (min, max) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

// find nearest number in array
export const NearestNo = arr => val =>
    arr.reduce(
        (p, n) => (Math.abs(p) > Math.abs(n - val) ? n - val : p),
        Infinity
    ) + val

// convert number to genre
export const IndextoGenre = (num, side) => genreList[side][num]

// reset volume
export const resetVolume = player => player.volume(howlerOptions.volume)

// get sweepstakes probability
export const getSweepsProbability = () => sweepChances[getRandomInt(0, 9)]

// convert degree to genre
export const SelectiontoNum = (selection, side) => {
    // index
    let index =
        side === 'left'
            ? leftRecordPoints.indexOf(selection)
            : rightRecordPoints.indexOf(selection)
    return index
}

// set record to
export const SetRecord = (side, id) => {
    TweenMax.set(`#${side === 'left' ? 'left' : 'right'}Record`, {
        rotation:
            side === 'left' ? leftRecordPoints[id] : rightRecordPoints[id],
    })
}

// animate genre in record
export const AnimateActiveGenre = (id, side) =>
    TweenMax.to(
        `#${side === 'left' ? 'left' : 'right'}Record`,
        randomSelectionSpeed,
        {
            rotation:
                side === 'left' ? leftRecordPoints[id] : rightRecordPoints[id],
            ease: Expo.easeOut,
        }
    )

// animate record
export const AnimateRecord = (side, angle, callback) =>
    TweenMax.to(
        `#${side === 'left' ? 'left' : 'right'}Record`,
        recordAnimateSpeed,
        {
            rotation: angle,
            ease: Expo.easeOut,
            onComplete: callback,
        }
    )

// select random genre
export const setRandomGenre = (currentLeft, currentRight) => {
    // play spin sound
    playSpinSound()
    // items
    let left, right
    // get left & right without duplicate
    do {
        left = getRandomInt(0, 4)
    } while (left === currentLeft)
    do {
        right = getRandomInt(0, 4)
    } while (right === currentRight)
    // animate record
    AnimateActiveGenre(left, 'left')
    AnimateActiveGenre(right, 'right')
    // return sides
    return { left, right }
}

// facebook sharer
export const socialShare = (id, platform, mashup) => {
    const twcopy = `Listen%20to%20the%20${id}%20mashup%20on`
    const social =
        platform === 'fb'
            ? 'https://www.facebook.com/sharer.php?u=' + sharePage(id)
            : `https://twitter.com/intent/tweet?text=${twcopy}&url=http://52.221.4.178/mashup_id.html#` +
              sharePage(id)
    const left = (window.screen.width - 570) / 2
    const top = (window.screen.height - 570) / 2
    const params =
        'menubar=no,toolbar=no,status=no,width=570,height=570,top=' +
        top +
        ',left=' +
        left
    window.open(social, 'NewWindow', params)
}

// form validation
export const validate = values => {
    const errors = {}
    const requiredFields = ['name', 'age', 'phone', 'email', 'consent']
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    if (values.age) {
        if (
            values.age === parseInt(values.age, 10) ||
            values.age.toString().length > 2
        ) {
            errors.age = 'Invalid age'
        }
    }
    if (values.phone && values.phone.length < 8) {
        errors.phone = 'Invalid phone number'
    }
    return errors
}

// play spin sound
export const playSpinSound = () => {
    // sound
    // const spinSound = new Howl({
    //     src: [require('../assets/sounds/spin.mp3')],
    //     loop: false,
    //     buffer: true,
    // })
    // spinSound.play()
}
