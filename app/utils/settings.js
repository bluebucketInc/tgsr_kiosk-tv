// settings
export const // sweep chances
    sweepChances = [1, 1, 1, 1, 3, 3, 3, 5, 5, 20],
    // record snaps
    leftRecordPoints = [0, 72, 144, 216, 288],
    rightRecordPoints = [0, 72, 144, 216, 288],
    // rightRecordPoints = [0, 63, 129, 209, 297],
    // animation
    randomSelectionSpeed = 2, // s
    randomPlayDelay = 2000, // ms
    AnimateDuration = 400, // ms
    recordAnimateSpeed = 1, // s
    // genre list
    genreList = {
        left: ['1300s', '1965-PRESENT', '1900-1945', '1819', '1400-1700'],
        right: ['POP', 'INDIE', 'JAZZ', 'ELECTRONIC', 'URBAN'],
    },
    // howlerOptions
    howlerOptions = {
        autoplay: false,
        loop: false,
        volume: 1,
        preload: false,
        html5: true,
    },
    // slick settings
    slickSettings = {
        dots: true,
        infinite: false,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        dotsClass: 'slick-dots onboarding-dots',
    },
    siteKey = '6LccnrwUAAAAAAbUKoF5IlWJ_h4IHcjUAE9UIja_',
    api = {
        gateway: `${
            process.env.NODE_ENV === 'development'
                ? 'http://52.221.4.178:7000'
                : ''
        }/api/tgsr/`,
        save: 'add-visitor-info',
        captcha: 'recaptcha',
    },
    sharePage = mashup => `http://52.221.4.178/mashup_id.html#${mashup}`
