// imports
import { createStore, applyMiddleware, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk'
import * as History from 'history'
import reducer from '../reducers/index.js'

// history object
export const history = History.createBrowserHistory()

// initial state
const initialState = {}
const enhancers = []
const middleware = [thunk, routerMiddleware(history)]

// id Dev
if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__
    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
}
const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
)

// store
export default createStore(
    connectRouter(history)(reducer),
    initialState,
    composedEnhancers
)
